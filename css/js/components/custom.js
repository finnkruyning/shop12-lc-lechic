$(function () {
    if (!$("#headerlogin").length) {
        $('.topbar-menu-right').find('#navbar-login').remove();
    }
    if (!$("#headerlogout").length) {
        $('.topbar-menu-right').find('#navbar-logout').remove();
    }
    $('#pagecontent').find('.visual').insertAfter($('.navbar'));

});

var lechic = (function($, window, undefined) {
    var $btnBlock = $("#body_account .button-block");

    function headerFixed() {
        /*------header-------*/
        $(window).scroll(function () {
            var w = $(window).width();
            if(w > 991){
                if ($(window).scrollTop() > 50) {
                    $("div#header").addClass("scroll");
                } else {
                    $("div#header").removeClass("scroll");
                }
            }
        });
    }

    function subMenuMobile() {
        $('.block-top .middlebar-menu-right .dropdown').click(function (e) {
            $(this).find('ul.dropdown-content').toggleClass('show');
            $(this).find('span').toggle();
        });
    }

    function moveMenuToBlock() {
        if($('.hgroup-top-m').length) {
            $('#template-sidebar .block-top .container').append($('.menu-top-m'));
        }
    }

    function scrollTopBtn() {
        //Check to see if the window is top if not then display button
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollToTop').fadeIn();
            } else {
                $('.scrollToTop').fadeOut();
            }
        });

        //Click event to scroll to top
        $('.scrollToTop').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });
    }

    function setContentFullWidth() {
        if($("#template-sidebar").hasClass("w-100")) {
            $("#categories").addClass("hide");
            $("#mid").addClass("w-100");
        }
    }

    function addClassActiveCart() {
        var $idex= $('.swiper-slide-active').index();
        $('.swiper-controls > span:eq("'+$idex+'")').addClass('active');
        $('.swiper-controls > span:not(:eq("'+$idex+'"))').removeClass('active');
    }

    function addTitleCateBodyDesign() {
        if($('#body_design').length) {
            /*addTitleCate*/
            var $tt = '<div class="block"><div class="hgroup hgroup-top-m text-center"><h1 class="uppercase main-title"><span>Ook interessant</span></h1><h5>Meer kaarten uit deze categorie</h5></div></div>'
            $($tt).insertBefore('#card_previews_horizontal');
            /*backOverView*/
            var $getCate = $('body').attr('class').split(' ')[0];
            var $getbreadcrum = $('.crumb-mobile a').attr('href');
            var $back = '<div class="button-group"><a class="back-over-view button button-extra button-xs" href="'+$getbreadcrum+'"><i class="fa fa-angle-left"></i>Terug naar overzicht</a></div>';
            $($back).insertBefore($('.box-primary'));
            $("#card_previews_horizontal").wrap('<div class="wrap_card_previews"></div>');
            addClassActiveCart();
            $('.swiper-controls > span').click(function () {
                addClassActiveCart();
            })
        }
    }

    function checkFullWidth() {
        if($('#template-sidebar.no-full').length) {
            $('#categories').addClass("show");
            $('#pagecontent, #mid.container').addClass("no-full");
        }
    }

    function addLogo() {
        if($('#body_login').length) {
            $('#pagecontent').prepend('<div class="wrap-logo text-center"><div class="container"><a class="logo" href="/" title="Hippe Geboortekaartjes"><img src="/img/logo.png" alt="LeChic"></a></div></div>');
        }
    }

    function fixSpace() {
        if($('.wrap-logo').length =="") {
            $('#mid').addClass('margin-top-50');
        }
    }

    function getNumCart() {
        if($('#navbar-basket').length!=0) {
            $('#navbar-basket').html($('#basket_count').html());
        }
    }

    function hideListAcc() {
        var $btnBlock = $("body[id^='body_account'] ul.list-vertical li a");
        $btnBlock.each(function(){
            switch ($(this).attr('href')) {
                case "/account_adresboek":
                case "/account_adres_import":
                    $(this).parent('li').remove();
                    break;
            }
        });

        var $accountPage = $("body[id='body_account'] #pagebody .button-block a");
        $accountPage.each(function(){
            switch ($(this).attr('href')) {
                case '/account_address':
                case '/account_adres_import':
                    $(this).parent().parent().remove();
                    break;
            }
        });
    }

    function equalHeight() {
        if($('.equaldiv').length) {
            if($(window).width() >= 992){
                $('.equaldiv').each(function () {
                    var $rightHight = $('.equaldiv .right').height();
                    if($('.equaldiv .left').height() < $rightHight) {
                        $('.equaldiv .left .wrap').height($rightHight-30);
                    }
                })
            }
        }
    }

    function setTextEditor() {
        var $bgText = $('#bg_select .sp-preview-inner');
        var $fgText = $('#fg_select .sp-preview-inner');
        if($bgText.html()=="") {
            $bgText.html('Wit');
        }

        if($fgText.html()=="") {
            $fgText.html('Zwart');
        }
    }

    function _sidebar() {
        if($('#sidebar')) {
            var $currentLeftMenu = $("#sidebar a[href='" + window.location.pathname + "']");
            var $currentParent = $currentLeftMenu.closest('li');
            $currentLeftMenu.addClass('active');

            $('#sidebar ul.list > li:not(.list-toggle)').each(function () {
                if($(this).has('ul')) {
                    $(this).has('ul').addClass('accordion');
                    $(this).has('ul').find('a').eq(0).append('<i class="icon icon-x1 icon-chevron-right"></i>');
                    $(this).children('ul').hide();
                    $(this).has("a[href='" + window.location.pathname + "']").children('ul').show();
                    $(this).has("a[href='" + window.location.pathname + "']").find('i').toggleClass('icon-chevron-right icon-chevron-down');
                }
            });

            $('#sidebar .accordion i').each(function () {
                $(this).click(function (e) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    $(this).toggleClass('icon-chevron-right icon-chevron-down');
                    $(this).closest('li').children('ul').toggle();
                });
            })

        }
    }

    return {
        Init : function() {
            headerFixed();
            subMenuMobile();
            moveMenuToBlock();
            scrollTopBtn();
            setContentFullWidth();
            addTitleCateBodyDesign();
            checkFullWidth();
            addLogo();
            getNumCart();
            fixSpace();
            hideListAcc();
            setTextEditor();
            _sidebar();
        },
    };
}(jQuery, window));

jQuery(document).ready(function() {
    lechic.Init();
});
