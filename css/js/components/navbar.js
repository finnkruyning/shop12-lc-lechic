$(function () {
    var $navbar = $('.navbar');

    $navbar.find('.navbar-search-field').hide();

    $navbar.find('.open-navbar-search').on('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var _self = $(this);
        var target = _self.closest($navbar).find('.navbar-search-field');

        target.toggle();

        target.on('click', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
        });

    });

    $navbar.find('.navbar-hamburger').on('click', function (e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($navbar).find('.collapse');

        target.toggle();

        if (target.is(":visible")) {
            $('html').css('overflowY', 'hidden');
        } else {
            $('html').css('overflowY', 'auto');
        }
    });

    $navbar.find('.dropdown').on('click', function () {
        /*e.preventDefault();*/
        /*var _self = $(this);
        var target = _self.closest('.dropdown').find('.dropdown-content');
        target.toggle();*/
        $(this).find('ul.dropdown-content').toggleClass('show');
        //$(this).find('ul.dropdown-content').toggleClass('show');
    });


    /*$navbar.find('.dropdown > a').on('mouseenter', function (e) {
        //e.preventDefault();
        var _self = $(this);
        var target = _self.closest('.dropdown').find('.dropdown-content');
        target.show();
    });*/

    $navbar.find('.js-dropdown-hover').on('mouseenter', function (e) {
        e.preventDefault();
        var _self = $(this);
        _self.closest('.js-dropdown').toggleClass('active');
        var target = _self.closest('.js-dropdown').find('.js-dropdown-content');
        target.toggle();
    });

    $navbar.find('.js-dropdown').on('mouseleave', function (e) {
        e.preventDefault();
        var _self = $(this);
        _self.closest('.js-dropdown').removeClass('active');
        var target = _self.closest('.js-dropdown').find('.js-dropdown-content');
        target.hide();
    });

    $navbar.find('.js-dropdown-click').on('click', function (e) {
        e.preventDefault();
        var _self = $(this);
        _self.closest('.js-dropdown').toggleClass('active');
        var target = _self.closest('.js-dropdown').find('.js-dropdown-content');
        target.toggle();
    });

    $navbar.find('.js-dropdown-click').on('mouseleave', function (e) {
        e.preventDefault();
        var _self = $(this);
        _self.closest('.js-dropdown').removeClass('active');
        var target = _self.closest('.js-dropdown').find('.js-dropdown-content');
        target.hide();
    });

    $(document).click(function () {
        $navbar.find('.navbar-search-field').hide();
    });

});
