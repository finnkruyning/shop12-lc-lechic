$(function () {
    var $thumbnailScroller = $('.thumbnail-scroller');

    $thumbnailScroller.each(function () {
        var _self = $(this);
        _self.find('.thumbnail-scroller-middle').cycle({
            fx: 'carousel',
            slides: '> .item',
            speed: 600,
            swipe: true,
            carouselFluid: true,
            sync: false,
        });

    });

    $thumbnailScroller.find('.thumbnail-scroller-right a').click(function (e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($thumbnailScroller).find('.thumbnail-scroller-middle');
        target.cycle('next');
    });

    $thumbnailScroller.find('.thumbnail-scroller-left a').click(function (e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($thumbnailScroller).find('.thumbnail-scroller-middle');
        target.cycle('prev');
    });

});

